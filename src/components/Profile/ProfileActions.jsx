import { Link } from "react-router-dom"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../context/UserContext"
import { storageDelete, storageSave } from "../../utils/storage"
import { translationClearHistory } from "../../api/translation"

const ProfileActions = () => {
    
    const { user,  setUser } = useUser()
    
    const handleLogoutClick = () => { 
        if (window.confirm('Are you sure?')) {
            storageDelete(STORAGE_KEY_USER)
            setUser(null)                       
        }
    }

    const handleClearHistoryClick =async() =>{
        if (!window.confirm ('Are you sure?\nThis can not be undone!')){
            return
        }
        const [clearError, result] = await translationClearHistory(user.id)
        console.log(result)
        if (clearError !== null){
            console.log("Error")
            //Do something about this!
            return
        }
        
        const updatedUser={
            ...user,
            translations: []
        } 
        storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser)
    }

    return (
        <ul>
            <li><Link to= "/translation">Translations</Link></li>
            <li><button onClick = { handleClearHistoryClick }>Clear history</button></li>
            <li><button onClick= { handleLogoutClick }>Logout</button></li>
        </ul>    
    )
}
export default ProfileActions