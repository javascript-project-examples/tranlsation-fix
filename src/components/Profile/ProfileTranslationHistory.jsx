import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"

const ProfileTranslationHistory = ({ translations }) => {
    let translation
    
    const translationList = translations.map(
        (translation, index) => <ProfileTranslationHistoryItem key={ index +'-'+ translation } translation={translation } />) 
    return (
        <section>
            <h4>Your translation history</h4>
            { translationList.length === 0  && <p>You have no translations notes yet.</p>}
            
            <ul>

                { translationList }
            </ul>        
        </section>        
    )
}
export default ProfileTranslationHistory 
