

const TranslationsSummary = ({letter }) => {

    const letters = letter.split('')

    const signs = letters.map((letter,key)=> {
        return <img src = {require(`../../img/${letter}.png`)} alt='a' width = "55" key={key}/>
    })
    

    return(
        <section>
            <h4>Translation Summary</h4>
            <div>
                 {signs}
            </div>            
        </section>
    )
}
export default TranslationsSummary