

const TranslationsSignLetter = ({ letter, onSelect }) => {
    console.log(letter);
    return (
        <button onClick={ () => onSelect(letter.id) }>
            <aside>
                <img src= { `img/${letter}.png` } alt= { letter.name } width="55" />
            </aside>
            <section>
                <b>{ letter.name }</b>
            </section>
        </button>        
    )
}
export default TranslationsSignLetter

