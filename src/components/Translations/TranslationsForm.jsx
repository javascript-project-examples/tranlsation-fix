import { useForm } from "react-hook-form"


const TranslationsForm = ({ onOrder }) => {

    const { register, handleSubmit } = useForm()

    const onSubmit = ({ translationsNotes }) =>{ onOrder(translationsNotes) }
    
    return(
        <form onSubmit={ handleSubmit(onSubmit) }>
            <fieldset>
                <label htmlFor="translation-notes">Translation notes:</label>
                <input type="text" {...register('translationsNotes')} placeholder=" ⌨️ |Hello, write here! " />
            </fieldset>

            <button type= "submit">Translation</button>
        </form>
    )
}
export default TranslationsForm