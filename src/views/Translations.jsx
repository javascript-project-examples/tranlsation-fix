import TranslationsSummary from "../components/Translations/TranslationsSummary"
import { useState } from "react"
import { translationAdd } from "../api/translation"
import TranslationsSignLetter from "../components/Translations/TranslationsSignLetter"
import TranslationsForm from "../components/Translations/TranslationsForm"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"
import { storageSave } from "../utils/storage"
import { STORAGE_KEY_USER } from "../const/storageKeys"

const LETTERS = [
    
]

const Translations = () => {

    const[ letter, setLetter ]= useState("null")
    const { user, setUser }= useUser()

    const handleLetterClicked = (letterId) => {
       setLetter(LETTERS.find(letter => letter.id === letterId))
    }


    const handleTranslationClicked = async (notes) => {
        console.log(notes,"clicked")

        setLetter(notes)

        // let translationNotes = notes.split("").map(letter => {
        //     //console.log(translationNotes)  får ej signimage
        //     return <TranslationsSignLetter  
        //                 letter={ letter }  
        //                 onSelect={ handleTranslationClicked }/>
        // })   

        // Check if we have a coffee
        // Combine the coffee with the notes
        const translation = (notes).trim()
        //console.log();

        const [ error, updatedUser] = await translationAdd(user, translation) 
        if (error !== null) {
            //Bad
            return
        }
        
        //keep UI state and Server state in Sync
        storageSave(STORAGE_KEY_USER, updatedUser)
        // update context state
        setUser(updatedUser)
        console.log('Error ', error);
        console.log('updatedUser', updatedUser);
        // Send a HTTP request
        // Be happy!
    }

    //sign bilder visas
       const availableLetters = LETTERS.map(letter => {
        return <TranslationsSignLetter 
                    key= { letter.id } 
                    letter={ letter } 
                    image={ letter.image } 
                    onSelect={ handleLetterClicked }/>
    })   

    //Hello.split("")
    //[H, e, l ,l, o] 

    //´img/${letter}.png´

    return (
        <>
            <h1>Translation</h1>
            <section id ="translate letter-options">
                {  availableLetters }
            </section> 
            
            <section id="translation-notes">
                <TranslationsForm onOrder={ handleTranslationClicked }/>                
            </section>
            
            { letter && < TranslationsSummary letter = { letter } /> }        
        </>        
    )
}
export default withAuth (Translations)